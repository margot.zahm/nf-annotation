#!/usr/bin/env nextflow
/*
========================================================================================
    			       A N N O T A T I O N   P I P E L I N E
========================================================================================
 Annotation Pipeline. Started June 2019.
 Based on the work of Cedric CABAU <cedric.cabau@inra.fr>
 #### Authors
 Margot ZAHM <margot.zahm@inra.fr>
----------------------------------------------------------------------------------------
*/

def helpMessage() {
    log.info"""
    =========================================
         Annotation Pipeline v${params.version}
    =========================================
    Usage:
    The typical command for running the pipeline is as follows:
    nextflow run annotation --assembly 'assembly.fa' --reads 'path/to/rnaseq/*R{1,2}*.fastq.gz'  
    Mandatory arguments:
      --assembly            Fasta file of genome assembly to polish

      --reads               RNAseq fastq files to use for annotation

      --lineage             Lineage dataset used for BUSCO
    
    Options:
	  --no_masking          If specified, masking step is discarded.

      --species             Name used for Repbase and as prefix. Use '_' instead of spaces (default: 'Eukaryota')

      --stranded            If RNAseq reads are stranded

      --ref_db_exonerate    Directory with reference protein fasta files 
      						(default value : '/save/sigenae/data/Annotation/Fish_genomes_protein_reference_database/Unique_split_files/*.fa')
	  or
	  --exonerate_gff       Path to GFF file of homology evidences found with exonerate on the assembly. This parameter discards homology evidences step.

	  --cufflinks_gff       Path to cufflinks ouput file(s).

      --augustus_config     Augustus config file (default : '/work/project/sigenae/Cedric/Project_GenoFish_Genomes/softwares/augustus_custom_extrinsic.cfg')
	  or
	  --braker_dir          Path to braker output directory.

      --rmodeler_cons       Path to consensi.fa.classified from RepeatModeler. This parameter disable RepeatModeler process.

      --useexisting         If braker was already run with the prefix you use, you should run this to avoid any error

      --fix_nucleotides     Maker only support sequences with nucleotides ATCG. If your assembly contains other nucleotides, this parameter prevents maker
                            from crashing but maker replaces all unsupported nucleotides by 'N'

      --outdir              The output directory where the results will be saved (default value : './')
    """.stripIndent()
}

params.help = false
if (params.help){
    helpMessage()
    exit 0
}

if (!params.assembly) {
	exit 1, "You must specify an assembly using --assembly"
} else if (!params.no_masking) {
	Channel.fromPath(params.assembly)
	       .into{Assembly; Assembly4Database; Assembly4Rmself; Assembly4Masking}
} else {
	Channel.fromPath(params.assembly)
	       .into{MaskedAssembly4SamIndex; MaskedAssembly4Exonerate; MaskedAssembly4Braker; MaskedAssembly4Maker; MaskedAssembly4STARIndex}
}

if (!params.reads) {
	exit 1, "You must specify an assembly using --reads"
} else {
	Reads = Channel.fromFilePairs(params.reads)
	               .ifEmpty { error "Cannot find any file matching: ${params.reads}"  }
}

if (params.lineage) {
	BuscoLineage = Channel.fromPath(params.lineage)
} else {
	exit 1, "You must specify a busco lineage using --lineage parameter"
}

Species = Channel.value(params.species)

Prefix = Channel.value(params.prefix)

if (!params.stranded) {
	OutWigStrand = Channel.value('--outWigStrand Unstranded')
	OutSAMstrandField = Channel.value('--outSAMstrandField intronMotif')
	LibraryType = Channel.value('fr-unstranded')
} else {
	OutWigStrand = Channel.value('--outWigStrand Stranded')
	OutSAMstrandField = Channel.value('')
	LibraryType = Channel.value('fr-firststrand')
}

if (params.exonerate_gff) {
	ExonerateAln = Channel.fromPath(params.exonerate_gff)
} else {
	DBExonerate = Channel.fromPath(params.ref_db_exonerate)
}

if (params.cufflinks_gff) {
	Channel.fromPath(params.cufflinks_gff)
	       .into{TranscriptsGTF4Modif; TranscriptsGTF4GFF}
}

if (params.braker_dir) {
	Barkeroutput = Channel.fromPath(params.braker_dir)
} else {
	AugustusConf = Channel.fromPath(params.augustus_config)
}


if (params.rmodeler_cons) {
	Consensi = Channel.fromPath(params.rmodeler_cons)
} else {
	Consensi = Channel.empty()
}

if (params.useexisting) {
	UseExisting = Channel.value('--useexisting')
} else {
	UseExisting = Channel.value('')
}

if (params.fix_nucleotides) {
	FixNucleotide = Channel.value('-fix_nucleotides')
} else {
	FixNucleotide = Channel.value('')
}


/*
STEP 1 : MASKING
*/

if (!params.no_masking) {
	process check_database {
		executor 'local'
		
		input:
		val species from Species

		output:
		val database_species into DatabaseSpecies

		shell:
		database_species = species.replaceAll(/_/){ ' ' }
		'''
		perl /usr/local/bioinfo/src/RepeatMasker/RepeatMasker-4-0-7/util/queryRepeatDatabase.pl -species !{database_species} > database
		if ! [ -s database ]
		then
			exit 5
		fi
		'''
	}

	process fasta_split {
		input:
		file assembly from Assembly

		output:
		file "*.tfa" into SplitAssemblyDUST, SplitAssemblyTRF, SplitAssembly mode flatten

		script:
		"""
		fasta_split.pl ${assembly} 1 entries
		"""
	}

	process dust {
		module 'bioinfo/ncbi_cxx--18_0_0'
		label 'small'

		input:
		file fasta_split from SplitAssemblyDUST

		output:
		file "*.dust" into DustMasker

		script:
		"""
		/usr/local/bioinfo/src/NCBI_tools++/ncbi_cxx--18_0_0/bin/dustmasker -in ${fasta_split} -out ${fasta_split}.dust -outfmt interval
		"""
	}

	process merge_dust {
		publishDir "${params.outdir}/01-Masking"
		executor 'local'

		input:
		file dust from DustMasker.collect()

		output:
		file "dustmasker.gff3" into DustGFF

		script:
		"""
		cat ${dust} > dustmasker.dust
		repeat_to_gff.pl dustmasker.dust
		mv dustmasker.dust.gff dustmasker.gff3
		"""
	}

	process trf {
		label 'small'
		module 'bioinfo/trf-v4.09'
		validExitStatus 0,1

		input:
		file fasta_split from SplitAssemblyTRF

		output:
		file "*.500.dat" into TRF

		script:
		"""
		/usr/local/bioinfo/src/TRF/trf-v4.09/trf409.linux64 ${fasta_split} 2 7 7 80 10 50 500 -d -h
		"""
	}

	process merge_trf {
		publishDir "${params.outdir}/01-Masking"
		executor 'local'

		input:
		file trf from TRF.collect()

		output:
		file 'trf.gff3' into TrfGFF

		script:
		"""
		cat ${trf} > trf.dat
		repeat_to_gff.pl trf.dat
		mv trf.dat.gff trf.gff3
		"""	
	}

	process build_database {
		module 'bioinfo/RepeatModeler-open-1.0.11'
		executor 'local'

		input:
		val species from Species
		file fasta from Assembly4Database

		output:
		file "${species}.*" into Database

		when:
		!params.rmodeler_cons

		script:
		"""
		BuildDatabase -name ${species} -engine ncbi ${fasta}
		"""
	}

	process rmodeler {
		module 'bioinfo/RepeatModeler-open-1.0.11'

		input:
		val species from Species
		file database from Database.collect()

		output:
		file 'RM_*/consensi.fa.classified' into Rmodeler

		when:
		!params.rmodeler_cons

		script:
		"""
		RepeatModeler -pa 8 -engine ncbi -database ${species}
		"""
	}

	process rmself {
		label 'large'
		module 'bioinfo/RepeatMasker-4-0-7'

		input:
		file classified from Rmodeler.mix(Consensi)
		file fasta from Assembly4Rmself

		output:
		file "${fasta}.masked" into FastaSelfMasked
		file "${fasta}.out" into FastaSelfOut

		script:
		"""
		RepeatMasker -pa 16 -xsmall -lib ${classified} ${fasta}
		"""
	}

	process rmfull {
		label 'large'
		module 'bioinfo/RepeatMasker-4-0-7'

		input:
		val species from DatabaseSpecies
		file fasta_masked from FastaSelfMasked

		output:
		file "${fasta_masked}.out" into FastaFullMasked

		script:
		"""
		RepeatMasker -pa 16 -xsmall -species ${species} ${fasta_masked}
		"""
	}

	process generate_self_gff {
		executor 'local'
		module 'bioinfo/RepeatMasker-4-0-7'
		publishDir "${params.outdir}/01-Masking"

		input:
		file fasta_self_out from FastaSelfOut

		output:
		file 'RMasker_self_masked.gff3' into GFFSelfMasked

		shell:
		'''
		rmOutToGFF3.pl !{fasta_self_out} | perl -ane '$id; if(!/^\\#/){@F=split(/\\t/, $_);chomp $F[-1];$id++;$F[-1]="ID=$F[0]-$id;$F[-1]";$F[-1]=~s/ /%20/g;$_=join("\\t", @F)."\\n"} print $_' > RMasker_self_masked.gff3
		'''
	}

	process generate_full_gff {
		executor 'local'
		module 'bioinfo/RepeatMasker-4-0-7'
		publishDir "${params.outdir}/01-Masking"

		input:
		file fasta_full_masked from FastaFullMasked

		output:
		file 'RMasker_full_masked.gff3' into GFFFullMasked

		shell:
		'''
		rmOutToGFF3.pl !{fasta_full_masked} | perl -ane '$id; if(!/^\\#/){@F=split(/\\t/, $_);chomp $F[-1];$id++;$F[-1]="ID=$F[0]-$id;$F[-1]";$F[-1]=~s/ /%20/g;$_=join("\\t", @F)."\\n"} print $_' > RMasker_full_masked.gff3
		'''
	}

	process mask_genome {
		executor 'local'
		module 'bioinfo/bedtools-2.26.0:system/datamash-1.3'
		publishDir "${params.outdir}/01-Masking"

		input:
		file fa from Assembly4Masking
		file dust_gff from DustGFF
		file trf_gff from TrfGFF
		file self_gff from GFFSelfMasked
		file full_gff from GFFFullMasked
		val sp from Species

		output:
		file "${sp}.dna.masked.fa" into MaskedAssembly4SamIndex, MaskedAssembly4Exonerate, MaskedAssembly4Braker, MaskedAssembly4Maker, MaskedAssembly4STARIndex
		file 'percentage_masked.txt' into PercentageMasked

		shell:
		'''
		cat !{dust_gff} !{trf_gff} !{self_gff} !{full_gff} | grep -v '>' | sort -k1,1 -k4,4n | bedtools merge -i - > all_repeats.bed
		cat all_repeats.bed | bedtools maskfasta -soft -fi !{fa} -bed - -fo !{sp}.dna.masked.fa
		m=\$(cat all_repeats.bed | awk '{print \$3-\$2+1}'| datamash sum 1)
		w=\$(cat !{fa} | fasta_length.pl | datamash sum 2)
		echo $m | awk -v w=$w '{printf "whole:%d\\nmasked:%d (%.2f%%)\\n",w,\$1,\$1*100/w}'| thousandify.pl > percentage_masked.txt
		'''
	}
}

process samtools_faidx {
	executor 'local'
	module 'bioinfo/samtools-1.8'

	input:
	file fasta from MaskedAssembly4SamIndex

	output:
	file '*.fai' into MaskedAssemblyFaidx

	script:
	"""
	samtools faidx ${fasta}
	"""
}

/*
STEP 2 : RNA-Seq evidences
*/

if (!params.cufflinks_gff) {
	process star_index {
		label 'large'
		module 'bioinfo/STAR-2.5.1b'

		input:
		file fasta from MaskedAssembly4STARIndex

		output:
		file 'STAR_index' into STARIndex

		script:
		"""
		mkdir STAR_index
		STAR --runThreadN $task.cpus --runMode genomeGenerate --genomeDir STAR_index --genomeFastaFiles ${fasta}
		"""
	}

	process star_mapping {
		label 'medium'
		module 'bioinfo/STAR-2.5.1b'
		publishDir "${params.outdir}/02-RNA-Seq", pattern: "*.Log.final.out"

		input:
		set val(pair_id), file(reads) from Reads
		file star_index from STARIndex.collect()
		val outWigStrand from OutWigStrand
		val outSAMstrandField from OutSAMstrandField
		val species from Species

		output:
		file '*.Log.final.out' into SaveMapStats
		set val(pair_id), file('*.bam') into BAMReads

		script:
		"""
		STAR --genomeDir ${star_index} --readFilesIn ${reads} --outFileNamePrefix ${species} --limitBAMsortRAM 32212254720 --readFilesCommand zcat\
		--runThreadN $task.cpus --outSAMtype BAM SortedByCoordinate --outWigType wiggle --outSAMattrIHstart 0 --outFilterIntronMotifs RemoveNoncanonical\
		--alignSoftClipAtReferenceEnds No ${outWigStrand} ${outSAMstrandField}
		mv ${species}Aligned.sortedByCoord.out.bam ${species}Aligned_${pair_id}.sortedByCoord.out.bam
		mv ${species}Log.final.out ${pair_id}.Log.final.out
		"""
	}

	process duplicates {
		module 'bioinfo/picard-2.18.2'

		input:
		set val(pair_id), file(reads) from BAMReads

		output:
		file '*.bam' into BAMNoDup4Merge
		set val(pair_id), file("${pair_id}_nodup.bam") into BAMNoDup4Cufflink

		script:
		"""
		java -Xmx56G -jar \$PICARD MarkDuplicates VALIDATION_STRINGENCY=LENIENT I=${reads} O=${pair_id}_nodup.bam M=${pair_id}.bam.metrics REMOVE_DUPLICATES=true
		"""
	}

	process merge_bam {
		label 'medium'
		module 'bioinfo/samtools-1.8'

		input:
		file bam from BAMNoDup4Merge.collect()

		output:
		file 'all_samples.bam' into BAMMerged4Wig, BAMMerged4Hints

		script:
		"""
		samtools merge -@ $task.cpus all_samples.bam ${bam}
		"""
	}

	process bam2wig {
		label 'medium'
		module 'bioinfo/STAR-2.5.1b'

		input:
		file bam from BAMMerged4Wig
		val outWigStrand from OutWigStrand

		output:
		file 'Signal.Unique.str1.out.wig' into UniqueWigAligned1
		file 'Signal.Unique.str2.out.wig' optional true into UniqueWigAligned2
		file 'Signal.UniqueMultiple.str1.out.wig' into MultipleWigAligned1
		file 'Signal.UniqueMultiple.str2.out.wig' optional true into MultipleWigAligned2

		script:
		"""
		STAR --runMode inputAlignmentsFromBAM --inputBAMfile ${bam} --outWigType wiggle ${outWigStrand}
		"""
	}

	process bam2hints {
		label 'medium'
		module 'bioinfo/augustus-3.3'

		input:
		file bam from BAMMerged4Hints

		output:
		file 'hints.rnaseq.intron.gff' into IntronRnaseq

		script:
		"""
		bam2hints --intronsonly --in=${bam} --out=hints.rnaseq.intron.gff
		"""
	}

	if (params.stranded) {
		process wig2hints_stranded {
			label 'medium'
			module 'bioinfo/augustus-3.3'

			input:
			file unique_str1 from UniqueWigAligned1
			file unique_str2 from UniqueWigAligned2

			output:
			file 'hints.rnaseq.exonpart*.gff' into ExonRnaSeq

			script:
			""" 
			cat ${unique_str1} | wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 --src=W --type=ep --radius=4.5 --pri=4 --strand='+' > hints.rnaseq.exonpart1.gff
			cat ${unique_str2} | wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 --src=W --type=ep --radius=4.5 --pri=4 --strand='-' > hints.rnaseq.exonpart2.gff
			"""
		}
	} else {
		process wig2hints_unstranded {
			label 'medium'
			module 'bioinfo/augustus-3.3'

			input:
			file unique_str1 from UniqueWigAligned1

			output:
			file 'hints.rnaseq.exonpart*.gff' into ExonRnaSeq

			script:
			"""
			cat ${unique_str1} | wig2hints.pl --width=10 --margin=10 --minthresh=2 --minscore=4 --prune=0.1 --src=W --type=ep --radius=4.5 --pri=4 --strand='.' > hints.rnaseq.exonpart1.gff
			"""
		}
	}

	process cufflinks {
		label 'medium'
		module 'bioinfo/cufflinks-2.2.1'
		publishDir "${params.outdir}/02-RNA-Seq"

		input:
		set val(pair_id), file(bam) from BAMNoDup4Cufflink
		val library_type from LibraryType

		output:
		file "${pair_id}_transcripts.gtf" into TranscriptsGTF4GFF, TranscriptsGTF4Modif

		script:
		"""
		cufflinks --no-update-check --max-frag-multihits 10 --max-bundle-length 5000000 --max-bundle-frags 1000000 --max-intron-length 100000 --num-threads $task.cpus --overlap-radius 5 --library-type ${library_type} ${bam}
		mv transcripts.gtf ${pair_id}_transcripts.gtf
		"""
	}
}

process cufflinks2gff3 {
	executor 'local'
	module 'bioinfo/maker-3.01.02-beta'

	input:
	file transcripts_gtf from TranscriptsGTF4GFF.collect()

	output:
	file 'cufflinks_aln.gff' into CufflinksAln

	script:
	"""
	cufflinks2gff3 ${transcripts_gtf} > cufflinks_aln.gff
	"""
}

/*
STEP 3 : Homology evidences
*/

if (!params.exonerate_gff) {
	process exonerate {
		module 'bioinfo/exonerate-2.2.0'

		input:
		file db from DBExonerate
		file fasta from MaskedAssembly4Exonerate.collect()

		output:
		file "*.exonerate" into Exonerate

		script:
		"""
		module load bioinfo/exonerate-2.2.0
		exonerate --model protein2genome --showvulgar yes --showalignment no --showquerygff no --showtargetgff yes --softmasktarget yes --percent 80 --ryo 'AveragePercentIdentity: %pi\\n' --maxintron 30000 ${db} ${fasta} > \$(basename ${db} .fa).exonerate
		"""
	}

	process merge_exonerate {
		executor 'local'

		input:
		file exonerate from Exonerate.collect()

		output:
		file 'exonerate.gff' into ExonerateAll

		script:
		"""
		cat ${exonerate} > exonerate.gff
		"""
	}

	process exonerate2gff3 {
		executor 'local'
		publishDir "${params.outdir}/03-Exonerate"

		input:
		file exonerate_gff from ExonerateAll

		output:
		file 'exonerate_aln.gff' into ExonerateAln

		script:
		"""
		exonerate_to_maker_gff3.pl ${exonerate_gff} > exonerate_aln.gff
		"""
	}
}

/*
STEP 4 : De novo evidences
*/

if (!params.braker_dir) {
	process gtf2gff {
		executor 'local'
		module 'bioinfo/genometools-1.5.10'

		input:
		file gtf from TranscriptsGTF4Modif

		output:
		file "${gtf}_modified.gff" into ModifiedGFF

		shell:
		'''
		cat !{gtf} | awk '\$3=="exon"' | perl -laF"\\t" -ne '$F[8]=~s/gene_id "(.+?)"/gene_id "$n.\$1"/;$F[8]=~s/transcript_id "(.+?)"/transcript_id "$n.\$1"/;$F[8]=~s/FPKM/fpkm/;print join("\\t",@F)' | gt gtf_to_gff3 | gt gff3 -addintrons > !{gtf}_modified.gff
		'''
	}

	process merge_gff {
		executor 'local'

		input:
		file gff from ModifiedGFF.collect()

		output:
		file 'all_transcripts.gff' into AllTranscriptsGff4Exon, AllTranscriptsGff4Intron

		script:
		"""
		cat ${gff} > all_transcripts.gff
		"""
	}

	process generate_exon {
		executor 'local'

		input:
		file gff from AllTranscriptsGff4Exon

		output:
		file 'hints.cdna.exon.gff' into ExoncDNA

		shell:
		'''
		cat !{gff} | awk '$3=="exon"' | perl -laF"\\t" -ne '
		$F[1]= "b2h"; $F[2]="exonpart"; $F[5]=10;
		$F[8]=~/Parent=(mRNA\\d+)/;
		$F[8]= "grp=\$1;pri=4;src=E";
		print join("\\t",@F);
		' > hints.cdna.exon.gff
		'''
	}

	process generate_intron {
		executor 'local'

		input:
		file gff from AllTranscriptsGff4Intron

		output:
		file 'hints.cdna.intron.gff' into IntroncDNA

		shell:
		'''
		cat !{gff} | awk '$3=="intron"' | perl -laF"\\t" -ne '
		$F[1]= "b2h"; $F[2]="intron"; $F[5]=10;
		$F[8]=~/Parent=(mRNA\\d+)/;
		$F[8]= "grp=$1;pri=4;src=E";
		print join("\\t",@F);
		' > hints.cdna.intron.gff
		'''
	}

	process merge_transcripts {
		executor 'local'

		input:
		file intron_rnaseq from IntronRnaseq
		file exon_rnaseq from ExonRnaSeq.collect()
		file intron_cdna from IntroncDNA
		file exon_cdna from ExoncDNA

		output:
		file 'hints.rnaseq.gff' into HintsRnaSeq

		script:
		"""
		cat ${intron_rnaseq} ${exon_rnaseq} ${intron_cdna} ${exon_cdna} | sed -e 's/\\tep\\t/\\texonpart\\t/' > hints.rnaseq.gff
		"""
	}

	process braker {
		module 'bioinfo/BRAKER_v2.0.4'
		publishDir "${params.outdir}/04-Braker", pattern: "braker"

		input:
		file fasta from MaskedAssembly4Braker
		file rna_gff from HintsRnaSeq
		file augustus_cfg from AugustusConf
		val sp from Species
		val useexisting from UseExisting

		output:
		set file("braker/${sp}/augustus.gff"), file("braker/${sp}/augustus.aa") into GenerateSupportedaa
		file "braker/${sp}/augustus.gtf" into AugustusGTF
		file "braker/${sp}/augustus.aa" into BuscoAugustusAA
		file 'braker'

		script:
		"""
		braker.pl --cores $task.cpus --genome=${fasta} --species=${sp} --hints=${rna_gff} --softmasking --extrinsicCfgFile=${augustus_cfg} --UTR=off ${useexisting}
		"""
	}
} else {
	process extract_braker_data {
		input:
		file braker_output from Barkeroutput

		output:
		set file("${braker_output}/*/augustus.gff"), file("${braker_output}/*/augustus.aa") into GenerateSupportedaa
		file "${braker_output}/*/augustus.gtf" into AugustusGTF
		file "${braker_output}/*/augustus.aa" into BuscoAugustusAA

		script:
		"""

		"""
	}
}

process generate_aa {
	executor 'local'

	input:
	set file(augustus_gff), file(augustus_aa) from GenerateSupportedaa

	output:
	file 'augustus.supported.aa' into SupportedTranscripts

	shell:
	'''
	cat !{augustus_gff} | perl -laF"\\t" -ne '$name=$F[8] if ($F[2] eq "transcript"); if (/# % of transcript supported by hints \\(any source\\): (\\d+)/){print "$name\\t\$1"}' | awk '\$2>0' | cut -f1 > supported_transcripts.txt
	cat !{augustus_aa} | fasta_extract.pl supported_transcripts.txt > augustus.supported.aa
	'''
}

process gtf2gff3 {
	executor 'local'
	publishDir "${params.outdir}/04-Braker"

	input:
	file augustus_gtf from AugustusGTF

	output:
	file 'braker_augustus.gff3' into AugustusBraker

	shell:
	'''
	/usr/local/bioinfo/src/EVM/EVidenceModeler-1.1.1/EvmUtils/misc/augustus_GTF_to_EVM_GFF3.pl !{augustus_gtf} > braker_augustus.gff
	(echo '##gff-version 3' && \
 	cat braker_augustus.gff | \
 	perl -laF"\\t" -ne '($id)=$F[8]=~/ID=([^;]+)/; $F[8]=~s/Augustus%20prediction/$id/ if ($F[8]=~/Name=Augustus%20prediction/); print join("\\t",@F)')\
 	> braker_augustus.gff3
	'''
}

/*
STEP 5 : Annotation with MAKER
*/

process maker {
	publishDir "${params.outdir}/05-Maker"

	input:
	val sp from Species
	val fix_nucleotides from FixNucleotide
	file genome from MaskedAssembly4Maker
	file est_gff from CufflinksAln
	file protein_gff from ExonerateAln
	file pred_gff from AugustusBraker

	output:
	file 'maker_bopts.ctl' into Makerbopts
	file 'maker_evm.ctl' into Maverevm
	file 'maker_exe.ctl' into Makerexe
	file 'maker_opts.ctl' into Makeropts
	file "${sp}.maker.output/${sp}_master_datastore_index.log" into MakerIndexLog
	file "${sp}.maker.output/${sp}.all.maker.proteins.fasta" into MakerProtein
	file "${genome}" into GenomeGenerateFiles

	script:
	template 'maker.sh'
}

process maker_output {
	executor 'local'
	module 'bioinfo/maker-3.01.02-beta'

	input:
	val sp from Species
	file maker_index_log from MakerIndexLog

	output:
	file "${sp}.all.maker.gff" into MakerGff
	file "${sp}.all.maker.noseq.gff" into MakerNoSeq

	script:
	"""
	gff3_merge -s -d ${maker_index_log} > ${sp}.all.maker.gff
	fasta_merge -o ${sp} -d ${maker_index_log}
	gff3_merge -n -s -d ${maker_index_log} > ${sp}.all.maker.noseq.gff
	"""
}

process unique_prot {
	executor 'local'
	module 'bioinfo/fastx_toolkit-0.0.14'

	input:
	val sp from Species
	file prot from MakerProtein

	output:
	file "${sp}.all.maker.proteins.fa" into MakerAllProteins, ProteinsAlternative4Busco

	shell:
	'''
	cat !{prot} | fasta_formatter -w 0 | perl -pe 's/(>.+)\n/$1\\t/' | sort -u | perl -pe 's/(>.+)\\t/$1\n/' | fasta_formatter -w 60 > !{sp}.all.maker.proteins.fa
	'''
}

Proteins4Busco = Channel.create()
Proteins4Busco << BuscoAugustusAA
Proteins4Busco << SupportedTranscripts
Proteins4Busco << ProteinsAlternative4Busco

process busco {
	input:
	file fasta from Proteins4Busco
	file lineage from BuscoLineage.collect()

	output:
	file "run_${fasta}/short_summary_${fasta}.txt" into BuscoAlternativeResults

	script:
	template 'busco.sh'
}

process select_best_transcript {
	executor 'local'

	input:
	file maker_noseq from MakerNoSeq

	output:
	file 'maker.no_alternatives.gff3' into MakerNoAlternative

	script:
	"""
	maker_GFF_to_single_transcript_per_gene_GFF.pl ${maker_noseq} > maker.no_alternatives.gff3
	"""
}

process rename_transcripts {
	executor 'local'
	publishDir "${params.outdir}/05-Maker"

	input:
	val sp from Species
	val prefix from Prefix
	file no_alternative from MakerNoAlternative

	output:
	file "${sp}.gff3" into FinalGFF

	shell:
	'''
	sed 1d !{no_alternatives} | cut -f1 | sort -u | sort -k1.14,1g | awk '{print $0"\\t"NR}' > maker.no_alternatives.chr.order.tsv
	myMaker_map_ids.pl --sort_order maker.no_alternatives.chr.order.tsv --prefix !{prefix} --abrv_gene G --abrv_tran T --justify 8 --initial 10 --increment 10 --iterate '' --suffix '' maker.no_alternatives.gff3 > maker.no_alternatives.id.map.tsv
	cat maker.no_alternatives.gff3 | sort -s -k1.14,1g > !{sp}.gff3
	myMap_gff_ids.pl --no_alias maker.no_alternatives.id.map.tsv !{sp}.gff3
	'''
}

process generate_last_files {
	executor 'local'
	module 'bioinfo/cufflinks-2.2.1'
	publishDir "${params.outdir}/05-Maker"

	input:
	val sp from Species
	file gff3 from FinalGFF
	file genome from GenomeGenerateFiles

	output:
	file "${sp}.proteins.fa" into FinalProteins
	file "${sp}.transcripts.fa" into FinalTranscripts
	file "${sp}.CDS.fa" into FinalCDS

	script:
	'''
	gffread -y !{sp}.proteins.fa -g !{genome} !{gff3}
	sed -i 's/\\.$//' !{sp}.proteins.fa
	gffread -w !{sp}.transcripts.fa -g !{genome} !{gff3}
	perl -i -pe 'tr/acgtn/ACGTN/ unless (/^>/)' !{sp}.transcripts.fa
	gffread -x !{sp}.CDS.fa -g !{genome} !{gff3}
	'''
}
