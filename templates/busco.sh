#!/bin/bash

module load system/Python-3.6.3 bioinfo/augustus-3.3 bioinfo/busco-3.0.2 system/R-3.4.3

run_BUSCO.py -i ${fasta} -l ${lineage} -m prot -c ${task.cpus} --out ${fasta}
