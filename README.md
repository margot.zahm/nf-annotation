# nf-annotation

Fishes annotation pipeline. The pipeline uses Nextflow. It finds repeat regions with various tools and annotates using 3 sources of evidences.

| Step               |
| ------------------ |
| Repeats annotation |
| RNA-Seq evidences  |
| Homology evidences |
| De novo evidences  |
| Annotation         |

## Dependencies
### Repeats annotation
* [Perl5](http://www.perl.org/get.html)
* Dustmasker from [ncbi_cxx toolkit](ftp://ftp.ncbi.nih.gov/toolbox/ncbi_tools++/CURRENT/) (tested version: 18.0.0)
* [Tandem Repeat Finder](https://tandem.bu.edu/trf/trf.download.html) (tested version: 4.09)
* [RepeatModeler](http://www.repeatmasker.org/RepeatModeler/) (tested version: 1.0.11)
* [RepeatMasker](http://www.repeatmasker.org/RMDownload.html) (tested version: 4.0.7)
* [RECON](http://www.repeatmasker.org/RepeatModeler/RECON-1.08.tar.gz) required by RepeatModeler and RepeatFinder
* [RepeatScout](http://www.repeatmasker.org/RepeatScout-1.0.6.tar.gz) required by RepeatModeler and RepeatFinder
* [RMBlast](http://www.repeatmasker.org/RMBlast.html) (recommended version: 2.9.0-p2 or higher) required by RepeatModeler and RepeatFinder
* [Bedtools](https://github.com/arq5x/bedtools2/releases) (tested version: 2.26.0)
* [Datamash](https://www.gnu.org/software/datamash/download/) (tested version: 1.3)

### RNA-Seq evidences
* [samtools](http://www.htslib.org/download/) (tested version: 1.8, 1.9)
* [STAR](https://github.com/alexdobin/STAR/releases/tag/2.5.1b) (tested version: 2.5.1b)
* [Picard](https://github.com/broadinstitute/picard/releases/tag/2.18.2) (tested version: 2.18.2)
* [Augustus](https://github.com/Gaius-Augustus/Augustus/releases/) [tested version: 3.3]
* [Cufflinks](https://github.com/cole-trapnell-lab/cufflinks/releases) (tested version: 2.2.1)

### Homology evidences
* [Exonerate](https://www.ebi.ac.uk/about/vertebrate-genomics/software/exonerate) (tested version: 2.2.0)
* [Genometools](http://genometools.org/pub/) (tested version: 1.5.10)

### De novo evidences
* [BRAKER](https://github.com/Gaius-Augustus/BRAKER/releases) (tested version: 2.0.4)

### Annotation
Not implemented yet.

## Input files
* Fasta file of the assembly to annotate. It must contain only ATCG nucleotides.
* RNA-Seq reads to use for **RNA-Seq evidences**
* Path to proteins fasta files for **Homology evidences**. For the fishes annotation, files are available at 
---`/save/sigenae/data/Annotation/Fish_genomes_protein_reference_database/Unique_split_files/*.fa` and was downloaded from Ensembl.

## Usage

```
nextflow run main.nf --assembly 'assembly.fa' --reads 'path/to/rnaseq/*R{1,2}*.fastq.gz'  
    Mandatory arguments:
      --assembly            Fasta file of genome assembly to polish

      --reads               RNAseq fastq files to use for annotation

      --lineage             Lineage dataset used for BUSCO

    Options:
      --species             Name used for Repbase and as prefix. Use '_' instead of spaces (default: 'Eukaryota')

      --stranded            If RNAseq reads are stranded

      --ref_db_exonerate    Directory with reference protein fasta files 
                            (default value : '/save/sigenae/data/Annotation/Fish_genomes_protein_reference_database/Unique_split_files/*.fa')

      --augustus_config     Augustus config file (default : '/work/project/sigenae/Cedric/Project_GenoFish_Genomes/softwares/augustus_custom_extrinsic.cfg')

      --rmodeler_cons       Path to consensi.fa.classified from RepeatModeler. This parameter disable RepeatModeler process.

      --useexisting         If braker was already run with the prefix you use, you should run this to avoid any error

      --fix_nucleotides     Maker only support sequences with nucleotides ATCG. If your assembly contains other nucleotides, this parameter prevents maker
                            from crashing but maker replaces all unsupported nucleotides by 'N'

      --outdir              The output directory where the results will be saved (default value : './')
```

## Output files
### Repeats annotation
All output files are stored in `01-Masking` directory:
* `species.dna.masked.fa`: assembly fasta file soft masked
* `dustmasker.gff3`: repeats found by dustmasker
* `trf.gff3`: repeats found by Tandem Repeat Finder
* `RMasker_full_masked.gff3` and `RMasker_self_masked.gff3`: repeats found by RepeatFinder
* `percentage_masked.txt`: percentage of input assembly masked

### RNA-Seq evidences
All output files are stored in `02-RNA-Seq` directory:
* `sample.Log.final.out`: Summary mapping statistics of STAR. There is one file per RNA-Seq paired reads.
* `sample_transcripts.gtf`: GTF file of transcripts delimited by cufflinks

### Homology evidences
All output files are stored in `03-Exonerate` directory:
* `exonerate.gff`: GFF file of transcripts determined by homology

### De novo evidences
All output files are stored in `04-Braker` directory:
* `braker_augustus.gff3`: GFF file of de novo predicted transcripts

### Annotation
Not implemented yet.
